![D85_2611-Edit](/Users/michaelhedl/Documents/clonehero_gitarre/pics/D85_2611-Edit.jpg)

# 3D printed wireless CloneHero guitar

> Michael Hedl
> 2021
>
> used to play the PC game [Clonehero](https://clonehero.net), a community driven clone of the legendary game [Guitar Hero](https://de.wikipedia.org/wiki/Guitar_Hero).

## Why?

Guitar Hero is not really supported by newer platforms. The community thus has built a game from scratch that plays exactly like the original, except is can be fully modded. Also custom songs can be added to the game. 

Guitar hero is played with hard to find special controllers, that only work for the intended platform. The Clone Hero Community plays the game with the standard keyboard flipped upside down and held like a guitar. The keys face away from the user. The left hand is placed below the keyboard that the F1-F5 keys can be reached. The other hand is used to hit the arrow keys. This way the feel of a Guitar Hero controller can be mimicked on a standard keyboard. 

Since the original controllers feel way more comfortable and precise, but are not compatible with PCs, I looked to find a way to build a custom wireless solution. 

## How?

This device is based on a old wireless XBox360 Controller. The controller is supported by Windows and many other operating systems. Also the game supports it very well. 

The controller is stripped down to the bare PCB. The button connections are soldered directly onto the PCB, shorting the right tracks for the Hardware to recognise the desired button press. That way any button can be used to trigger a button press on the XBox controller. Just connect the wireless dongle to the PCs USB port and you are ready to go.

I also 3D printed a nice housing. mounted all that to an off the shelf 2020 aluminium profile, and built in a battery and a micro USB charger. 

## Details...

### 3D Printed Parts

![f84e704a-a57b-4369-b105-86d37bd4c8d4](renders/f84e704a-a57b-4369-b105-86d37bd4c8d4.PNG)

### Button Connections

![210410_1509_ClonheroGuitar_2581](pics/210410_1509_ClonheroGuitar_2581.jpg)

![210410_1519_ClonheroGuitar_2597](pics/210410_1519_ClonheroGuitar_2597.jpg)



### Hardware "Bindings"

![210410_1516_ClonheroGuitar_2594](pics/210410_1516_ClonheroGuitar_2594.jpg)

All the PCB connections are routed to a screw terminal. This way the connections to the buttons can easily be changed afterwards, without touching the fragile soldered connections to the PCB. And they are labelled!

### Battery & Charging

![210410_1512_ClonheroGuitar_2590](pics/210410_1512_ClonheroGuitar_2590.jpg)

- A used LiPo battery from an old Laptop.
- TP4506 based battery charging PCB.
- Rocker 🤘 Switch

### PC Connection

![210410_1509_ClonheroGuitar_2581](pics/210410_1529_ClonheroGuitar_2614.jpg)

- Standard Wireless 360 Dongle for Windows.